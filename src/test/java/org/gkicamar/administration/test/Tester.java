package org.gkicamar.administration.test;

import javax.annotation.Resource;

import org.gkicamar.administration.data.model.Jemaat;
import org.gkicamar.administration.data.model.repo.JemaatModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.MongoRepository;

public class Tester {

//	@Autowired
//	private static MongoRepository<Jemaat, String> jemaatRepo;
	
	@Resource(name="jemaatModel")
	private static JemaatModel jemaatModel;
	
	public static void main(String[] args) {
//		ApplicationContext ctx = new GenericXmlApplicationContext("mongo-config.xml");
//		MongoOperations mongoOperations = (MongoOperations)ctx.getBean("mongoTemplate");
		System.out.println(jemaatModel);
		//fetch all jemaat
		System.out.println("Daftar Semua Jemaat:");
		System.out.println("====================");
		for(Jemaat jemaat : jemaatModel.getJemaat()){
			System.out.println(jemaat.getNama());
		}
		
	}

}
