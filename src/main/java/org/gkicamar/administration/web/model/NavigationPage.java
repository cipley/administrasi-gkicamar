package org.gkicamar.administration.web.model;

import java.io.Serializable;

public class NavigationPage implements Serializable {
	
	public NavigationPage() {
		super();
	}

	private Long id;
	private String title;
	private String subTitle;
	private String iconSclass;
	private String includeUri;
	
	public NavigationPage(String title, String subTitle, String iconSclass, String includeUri) {
		super();
		this.title = title;
		this.subTitle = subTitle;
		this.iconSclass = iconSclass;
		this.includeUri = includeUri;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getIconSclass() {
		return iconSclass;
	}

	public void setIconSclass(String iconSclass) {
		this.iconSclass = iconSclass;
	}

	public String getIncludeUri() {
		return includeUri;
	}

	public void setIncludeUri(String includeUri) {
		this.includeUri = includeUri;
	}
}
