package org.gkicamar.administration.web.vm;

import java.util.LinkedHashMap;
import java.util.Map;

import org.gkicamar.administration.web.model.NavigationPage;
import org.gkicamar.administration.web.template.common.SessionInitializer;
import org.gkicamar.administration.web.template.common.UserCredential;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Executions;

@Deprecated
@Component("NavigationVM")
@Scope("prototype")
public class NavigationVM {
	NavigationPage currentPage;
	private Map<String, Map<String, NavigationPage>> pageMap;
	
	@Init
	public void init(){
		UserCredential user = SessionInitializer.getSessionUserContext();
		
		if(user == null){
			Executions.sendRedirect("/login.zul");
			return;
		}
		
		initPageMap();
		currentPage = pageMap.get("Home").get("Halaman Depan");
	}
	
	@Command
	public void navigatePage(@BindingParam("target") NavigationPage targetPage){
		BindUtils.postNotifyChange(null, null, currentPage, "selected");
		currentPage = targetPage;
		BindUtils.postNotifyChange(null, null, this, "currentPage");
	}

	public NavigationPage getCurrentPage() {
		return currentPage;
	}

	public Map<String, Map<String, NavigationPage>> getPageMap() {
		return pageMap;
	}
	
	private void initPageMap(){
		pageMap = new LinkedHashMap<String, Map<String, NavigationPage>>();
		
		// TODO tambahin navigasi
		
		addPage("Home", "Halaman Depan", "/pages/blank.zul");
		
		addPage("Data Jemaat", "Umum", "");
		addPage("Data Jemaat", "Komisi", "");
		addPage("Data Jemaat", "Wilayah", "");
		
		addPage("Laporan", "Majelis", "");
		addPage("Laporan", "LKKJ Klasis", "");
	}
	
	private void addPage(String title, String subTitle, String includeUri){
		addPage(title, subTitle, includeUri, null);
	}
	
	private void addPage(String title, String subTitle, String includeUri, String data){
		Map<String, NavigationPage> subPageMap = pageMap.get(title);
		if (subPageMap == null) {
			subPageMap = new LinkedHashMap<String, NavigationPage>();
			pageMap.put(title, subPageMap);
		}
		NavigationPage navigationPage = new NavigationPage(title, subTitle,
				includeUri + "?random=" + Math.random(), data) {
			
//			@Override
//			public boolean isSelected() {
//				return currentPage == this;
//			}
		};
		subPageMap.put(subTitle, navigationPage);
	}
}
