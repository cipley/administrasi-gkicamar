package org.gkicamar.administration.web.vm;

import java.util.List;

import org.gkicamar.administration.data.model.Jemaat;
import org.gkicamar.administration.data.model.repo.JemaatModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.Selectors;

@Component("HomeJemaatVM")
@Scope("prototype")
public class HomeJemaatVM {
	
	private List<Jemaat> homeJemaatModel;
	
	@Autowired
	private JemaatModel jemaatModel;
	
	@AfterCompose
	@NotifyChange({"homeJemaatModel"})
	public void init(@ContextParam(ContextType.VIEW) org.zkoss.zk.ui.Component view ){
		Selectors.wireComponents(view, this, false);
		this.homeJemaatModel = jemaatModel.getJemaat();
	}
	
	public List<Jemaat> getHomeJemaatModel() {
		return homeJemaatModel;
	}

	public void setHomeJemaatModel(List<Jemaat> homeJemaatModel) {
		this.homeJemaatModel = homeJemaatModel;
	}
}
