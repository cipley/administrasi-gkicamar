package org.gkicamar.administration.web.template.common;

import org.gkicamar.administration.web.model.NavigationPage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SerializableEventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zkmax.zul.Nav;
import org.zkoss.zkmax.zul.Navbar;
import org.zkoss.zkmax.zul.Navitem;
import org.zkoss.zul.Include;

public class NavbarController extends SelectorComposer<Component> {
	private static final long serialVersionUID = 470419867872569241L;
	
	@Wire("#navbar")
	Navbar navbar;
	
	//wire service
	NavbarConfig navConfig = new NavbarConfigImpl();
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		Nav navAdministrator = new Nav();
		navAdministrator.setLabel("Administration");
		navAdministrator.setIconSclass("z-icon-desktop");
		navAdministrator.setParent(navbar);
		
		for(NavigationPage page : navConfig.getPages()) {
			System.out.println("Page label: " + page.getTitle());
			Navitem navItem = constructNavItem(page.getTitle(), page.getSubTitle(), page.getIconSclass(),
					page.getIncludeUri());
			navItem.setParent(navAdministrator);
		}
	}
	
	private Navitem constructNavItem(final String name, String label, String icon, final String href) {
		final Navitem nvi = new Navitem();
		
		nvi.setIconSclass(icon);
		nvi.setLabel(label);
		nvi.setHref(href);
		nvi.setClass(name);
		
		//new and register listener for events
		EventListener<Event> onActionListener = new SerializableEventListener<Event>() {
			private static final long serialVersionUID = 1L;
			public void onEvent(Event event) throws Exception {
				//redirect current url to new location
				if (href.startsWith("http")) {
					//opens a new browser tab
					Executions.getCurrent().sendRedirect(href);
				} else {
					//use iterable to find the first include only
					Include include = (Include) Selectors
							.iterable(navbar.getPage(), "#mainInclude")
							.iterator().next();
					include.setSrc(href);
					Sessions.getCurrent().setAttribute("CURRENT_PAGE", name);
					System.out.println("Name: " + name);
					
					if (name != null) {
						getPage().getDesktop().setBookmark(Context.BOOKMARK_SEPARATOR + name);
						nvi.setFocus(true);
						nvi.setSelected(true);
					}
				}
			}
		};
		nvi.addEventListener(Events.ON_CLICK, onActionListener);
		return nvi;
	}
}
