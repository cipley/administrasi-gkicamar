package org.gkicamar.administration.web.template.common;

import org.gkicamar.administration.web.model.NavigationPage;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.SerializableEventListener;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Navbar;
import org.zkoss.zkmax.zul.Navitem;
import org.zkoss.zul.A;
import org.zkoss.zul.Div;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Include;


public class NavbarComposer extends SelectorComposer<Component> {
	@Wire
	Hlayout main;
	@Wire
	Div sidebar;
	@Wire
	Navbar navbar;
	@Wire
	Navitem navitem;
	@Wire
	A toggler;
	
	//wire service
	NavbarPageConfig pageConfig = new NavbarPageConfigAjaxBasedImpl();
	
	public void doAfterCompose(Component comp) throws Exception {
		UserCredential user = SessionInitializer.getSessionUserContext();
		
		if (user == null) {
			Executions.sendRedirect("/login.zul");
			return;
		}
		
		for (NavigationPage page : pageConfig.getPages()) {
			Navitem navitem = constructNavItem(page.getTitle(), page.getSubTitle(), page.getIconSclass(), 
					page.getIncludeUri());
			if (!"search".equals(page.getTitle()))
				navbar.appendChild(navitem);
			String currentPage = (String) Sessions.getCurrent()
					.getAttribute(Context.CURRENT_PAGE);
			
			if (currentPage != null && currentPage.equals(page.getTitle())) {
				navitem.setSelected(true);
			} else {
				String indexPage = Executions.getCurrent().getDesktop().getRequestPath();
				if ("/index.zul".equals(indexPage) && "home".equals(page.getTitle()))
					navitem.setSelected(true);
			}
		}
	}
	
	//Sidebar toggle collapse/expand
	@Listen("onClick = #toggler")
	public void toggleNavbarCollapsed() {
		System.out.println(navbar.isCollapsed());
		if (navbar.isCollapsed()) {
			sidebar.setClass("sidebar");
			navbar.setCollapsed(false);
			toggler.setIconSclass("z-icon-angle-double-left");
		} else {
			sidebar.setClass("sidebar sidebar-min");
			navbar.setCollapsed(true);
			toggler.setIconSclass("z-icon-angle-double-right");
		}
		//forces the hlayout contains sidebar to recalculate its size
		Clients.resize(sidebar.getRoot().query("#main"));
	}
	
	private Navitem constructNavItem(final String name, String label, String icon, final String href) {
		final Navitem nvi = new Navitem();
		
		nvi.setIconSclass(icon);
		nvi.setLabel(label);
		nvi.setHref(href);
		nvi.setClass(name);
		
		//new and register listener for events
		EventListener<Event> onActionListener = new SerializableEventListener<Event>() {
			private static final long serialVersionUID = 1L;
			public void onEvent(Event event) throws Exception {
				//redirect current url to new location
				if (href.startsWith("http")) {
					//opens a new browser tab
					Executions.getCurrent().sendRedirect(href);
				} else {
					//use iterable to find the first include only
					Include include = (Include) Selectors
							.iterable(navbar.getPage(), "#mainInclude")
							.iterator().next();
					include.setSrc(href);
					Sessions.getCurrent().setAttribute("CURRENT_PAGE", name);
					System.out.println("Name: " + name);
					
					if (name != null) {
						getPage().getDesktop().setBookmark(Context.BOOKMARK_SEPARATOR + name);
						nvi.setFocus(true);
						nvi.setSelected(true);
					}
				}
			}
		};
		nvi.addEventListener(Events.ON_CLICK, onActionListener);
		return nvi;
	}
}
