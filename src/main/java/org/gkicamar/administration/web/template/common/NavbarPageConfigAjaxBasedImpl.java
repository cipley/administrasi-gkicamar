package org.gkicamar.administration.web.template.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.gkicamar.administration.web.model.NavigationPage;

public class NavbarPageConfigAjaxBasedImpl implements NavbarPageConfig {
	private HashMap<String, NavigationPage> pageMap = new LinkedHashMap<String, NavigationPage>();
	private UserCredential user;
	private void init() {
		user = SessionInitializer.getSessionUserContext();
		
		NavigationPage pageHome = new NavigationPage("home", "Home", "z-icon-home", "/home.zul");
		NavigationPage pageDataJemaat = new NavigationPage("data jemaat", "Data Jemaat", "z-icon-database", "");
		NavigationPage pageLaporan = new NavigationPage("laporan", "Laporan", "z-icon-file", "");
		
		pageMap.put(pageHome.getTitle(), pageHome);
		pageMap.put(pageDataJemaat.getTitle(), pageDataJemaat);
		pageMap.put(pageLaporan.getTitle(), pageLaporan);
	}
	
	public NavbarPageConfigAjaxBasedImpl() {
		init();
	}
	
	@Override
	public List<NavigationPage> getPages() {
		return new ArrayList<NavigationPage>(pageMap.values());
	}

	@Override
	public NavigationPage getPage(String name) {
		return pageMap.get(name);
	}

	@Override
	public UserCredential getUser() {
		return user;
	}

}
