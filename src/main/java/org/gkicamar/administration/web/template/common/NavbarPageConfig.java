package org.gkicamar.administration.web.template.common;

import java.util.List;

import org.gkicamar.administration.web.model.NavigationPage;

public interface NavbarPageConfig {
	List<NavigationPage> getPages();
	NavigationPage getPage(String name);
	UserCredential getUser();
}
