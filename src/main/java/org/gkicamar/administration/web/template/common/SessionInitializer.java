package org.gkicamar.administration.web.template.common;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Initiator;
import org.zkoss.zk.ui.util.InitiatorExt;

public class SessionInitializer extends BaseController implements Initiator, InitiatorExt {
	private final static Logger LOGGER = LoggerFactory.getLogger(SessionInitializer.class);
	
	@Override
	public void doInit(Page page, Map<String, Object> args) throws Exception {
		LOGGER.debug("init class");
		getSessionUserContext();
	}
	
	public static UserCredential getSessionUserContext() {
		UserCredential user = null;
		if(Sessions.getCurrent().getAttribute(Context.USER_CONTEXT) == null){
			LOGGER.debug("Unauthorized user");
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			UserDetails userDetails = (UserDetails) auth.getPrincipal();
			user = new UserCredential();
			user.setUserId(userDetails.getUsername());
			Sessions.getCurrent().setAttribute(Context.USER_CONTEXT, user);
		} else {
			user = (UserCredential) Sessions.getCurrent().getAttribute(Context.USER_CONTEXT);
		}
		return user;
	}
	
	@Override
	public void doAfterCompose(Page page, Component[] comps) throws Exception {}
	
	@Override
	public boolean doCatch(Throwable ex) throws Exception {
		return false;
	}

	@Override
	public void doFinally() throws Exception {}
}
