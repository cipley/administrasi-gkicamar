package org.gkicamar.administration.web.template.common;

import java.util.List;

import org.gkicamar.administration.web.model.NavigationPage;

public interface NavbarConfig {
	/** get pages of this application **/
	List<NavigationPage> getPages();
	
	/** get page **/
	NavigationPage getPage(String name);
	
	UserCredential getUser();
}
