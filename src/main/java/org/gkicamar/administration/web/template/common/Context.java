package org.gkicamar.administration.web.template.common;

public class Context {
	public static final String CURRENT_PAGE = "CURRENT_PAGE";
	
	public static final String BOOKMARK_SEPARATOR = "!";
	public static final String USER_CONTEXT = "USER_CONTEXT";
	public static final String CUSTOMER_CONTEXT = "CUSTOMER_CONTEXT";
	public static final String QUESTION_CONTEXT = "QUESTION_CONTEXT";
	
	public static final int DEFAULT_MAX_ROW_PAGING = 20;
	public static final int DEFAULT_MAX_ROW_PAGING_CS = 15;
}
