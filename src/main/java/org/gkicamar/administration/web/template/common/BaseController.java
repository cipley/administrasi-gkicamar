package org.gkicamar.administration.web.template.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;

public abstract class BaseController {
	private static final String NOTIFICATION_INFO = "Information";
	private static final String NOTIFICATION_CONFIRM = "Confirmation";
	
	private final static Logger LOGGER = LoggerFactory.getLogger(BaseController.class);
	
	protected UserCredential getUserContext(){
		UserCredential user = null;
		
		if(Sessions.getCurrent().getAttribute(Context.USER_CONTEXT) == null){
			Executions.getCurrent().sendRedirect("/login.zul");
		} else {
			user = (UserCredential) Sessions.getCurrent().getAttribute(Context.USER_CONTEXT);
		}
		return user;
	}
	
	protected String getUsername(){
		if(getUserContext() == null)
			return null;
		return getUserContext().getUserId();
	}
	
	protected void audit(Object object) {
		
		String result = object.toString();
		result = result.replaceAll("[\\t\\n\\r]"," ");
		
		LOGGER.info(result);
	}
	
	protected void alert(String message) {
    	
		Messagebox.show(message, 
    			NOTIFICATION_INFO, 
				Messagebox.OK, 
				Messagebox.ERROR);
    	
//    	Clients.evalJavaScript("playSound();");
    }
	
	protected void info(String message) {
		
    	Messagebox.show(message, 
    			NOTIFICATION_INFO, 
				Messagebox.OK, 
				Messagebox.INFORMATION);
    	
//    	Clients.evalJavaScript("playSound();");
    }
	
	protected void confirm(String confirmationMessage, EventListener<Event> callbackEvent) {
		
		Messagebox.show(confirmationMessage, 
				NOTIFICATION_CONFIRM,
				Messagebox.OK | Messagebox.CANCEL,
				Messagebox.QUESTION, 
				callbackEvent
				);
	}
}
