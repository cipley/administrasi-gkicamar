package org.gkicamar.administration.page.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.gkicamar.administration.data.model.Jemaat;
import org.gkicamar.administration.data.model.repo.JemaatModel;
import org.hibernate.annotations.Where;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("jemaatModel")
@Transactional
public class JemaatModelImpl implements JemaatModel {

	@Resource(name="mongoTemplate")
	private MongoTemplate mongoTemplate;
	
	@Override
	public List<Jemaat> getJemaat() {
		List<Jemaat> model = new ArrayList<>();
		try {
			model = mongoTemplate.findAll(Jemaat.class);
		} catch (Exception ex){
			ex.printStackTrace();
		}
		return model;
	}
}
