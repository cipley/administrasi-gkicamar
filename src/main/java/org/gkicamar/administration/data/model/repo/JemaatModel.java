package org.gkicamar.administration.data.model.repo;

import java.util.List;

import org.gkicamar.administration.data.model.Jemaat;

public interface JemaatModel {
	List<Jemaat> getJemaat();
}
