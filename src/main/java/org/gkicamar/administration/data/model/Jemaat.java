package org.gkicamar.administration.data.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "mst_dbaj")
public class Jemaat implements Serializable {
	
	private static final long serialVersionUID = 2280039736071787408L;

	public Jemaat() {
		super();
	}
	
	@PersistenceConstructor
	public Jemaat(String id, String nama, String noAnggota, String alamat, String tanggalLahir,
			String statusAnggota) {
		super();
		this.id = id;
		this.nama = nama;
		this.noAnggota = noAnggota;
		this.alamat = alamat;
		this.tanggalLahir = tanggalLahir;
		this.statusAnggota = statusAnggota;
	}
	
	@Id
	private String id;
	
	@Indexed
	private String nama;
	
	private String noAnggota;
	private String alamat;
	private String tanggalLahir;
	private String statusAnggota;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNoAnggota() {
		return noAnggota;
	}

	public void setNoAnggota(String noAnggota) {
		this.noAnggota = noAnggota;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getTanggalLahir() {
		return tanggalLahir;
	}

	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}

	public String getStatusAnggota() {
		return statusAnggota;
	}

	public void setStatusAnggota(String statusAnggota) {
		this.statusAnggota = statusAnggota;
	}
	
}
